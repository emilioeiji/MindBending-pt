Mind Bending
============

Artigos em Portugês do Brasil para o site `Mind Bending`_.

Se você quiser contribuir com algum artigo ou correção sinta-se livre para enviar um Pull-request.

Tecnologias
-----------

Todos os artigos são escritos utilizando a linguagem de marcação `reStructured Text`_ e são compilados para HTML com o auxílio do `Pelican`_.

Diretórios
----------

Abaixo uma breve descrição sobre o conteúdo de cada diretório:

- **audio:** Qualquer áudio que será referenciado no site, com exceção dos episódios do podcast Hack 'n' Cast;
- **codes:** Códigos a serem disponibilizados para download deve ser colocados aqui;
- **images:** Toda e qualquer imagem referenciada nos artigos deve se encontrar nesse diretório. Ele está em processo de organização e categorização;
- **pages:** Páginas do site, como sobre-mim, projetos, e etc;
- **articles:** Artigos do site. Cada artigo está dentro de um diretório que corresponde a sua categoria;
- **extra:** Diretório contendo adequações de tema e robots.txt. Não deve ser modificado;

Imagens
-------

Algumas imagens referenciadas nos artigos podem possuir direitos autorais ou licenças não compatíveis com a do site, a estas são reservados todos os diretos do autor.

Licensa
-------

Todos os **artigo** do Mind Bending Blog estão sob a licença `Creative Commons Attribution-ShareAlike 4.0`_ International (CC BY-SA 4.0).

.. _Mind Bending: http://mindbending.org/pt
.. _reStructured Text: http://docutils.sourceforge.net/rst.html
.. _Pelican: http://blog.getpelican.com/
.. _Creative Commons Attribution-ShareAlike 4.0: http://creativecommons.org/licenses/by-sa/4.0/
